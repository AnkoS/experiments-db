﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using ExperimentsGUI;

namespace UnitTests
{
    [TestClass]
    public class ApproximationTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestApproximationIfNullReferenceExceptionIsThrown()
        {
            Approximation app = new RegressionApproximation();
            int PolD = 1;

            List<double> res = app.Approximate(null, null, PolD);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestApproximationIfCoordinatesListIsEmptyExceptionIsThrown()
        {
            Approximation app = new RegressionApproximation();
            List<double> X = new List<double>();
            List<double> Y = new List<double>();
            int PolD = 1;

            List<double> res = app.Approximate(X, Y, PolD);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestApproximationIfPolynomialDegreeIsNotPositiveExceptionIsThrown()
        {
            Approximation app = new RegressionApproximation();
            List<double> X = new List<double> { 1, 2, 3 };
            List<double> Y = new List<double> { 4, 5, 6 };
            int PolD = -1;

            List<double> res = app.Approximate(X, Y, PolD);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestApproximationIfLengthOfListsIsNotEqualExceptionIsThrown()
        {
            Approximation app = new RegressionApproximation();
            List<double> X = new List<double> { 1, 2, 3 };
            List<double> Y = new List<double> { 4, 5, 6, 7 };
            int PolD = 1;

            List<double> res = app.Approximate(X, Y, PolD);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestApproximationIfLengthPolynomialDegreeIsNotCorrectExceptionThrown()
        {
            Approximation app = new RegressionApproximation();
            List<double> X = new List<double> { 1, 2, 3 };
            List<double> Y = new List<double> { 4, 5, 6 };
            int PolD = 3;

            List<double> res = app.Approximate(X, Y, PolD);
        }

        [TestMethod]
        public void TestApproximationIfResultIsCorrect()
        {
            Approximation app = new RegressionApproximation();
            List<double> X = new List<double> { 0, 1, 2 };
            List<double> Y = new List<double> { 0, 1, 4 };
            int PolD = 2;
            List<double> correctRes = new List<double> { 0, 0, 1 };

            List<double> res = app.Approximate(X, Y, PolD);

            Assert.IsTrue(correctRes.SequenceEqual(res));
        }
    }
}
