﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;

namespace ExperimentsDB
{
    public class Experimentator
    {
        public Experimentator()
        {
            Experiments = new List<Experiment>();
        }

        public Experimentator(String login, String firstname, String lastname, String middlename, String pw)
        {
            Login = login;
            FirstName = firstname;
            LastName = lastname;
            MiddleName = middlename;
            SHA256CryptoServiceProvider cryptoProvider = new SHA256CryptoServiceProvider();
            password = Encoding.ASCII.GetString(cryptoProvider.ComputeHash(Encoding.ASCII.GetBytes(pw)));
            Experiments = new List<Experiment>();
        }

        public Experimentator(Experimentator e)
        {
            Login = e.Login;
            FirstName = e.FirstName;
            LastName = e.LastName;
            MiddleName = e.MiddleName;
            Password = e.Password;
            Experiments = e.Experiments;
        }

        [Key]
        public String Login { set; get; }

        public String FirstName { set; get; }
        public String LastName { set; get; }
        public String MiddleName { set; get; }

        private String password;

        public String Password
        {
            private set
            {
                password = value;
            }
            get
            {
                return password;
            }
        }

        public static bool operator ==(Experimentator e1, Experimentator e2)
        {
            return (e1.FirstName == e2.FirstName && e1.MiddleName == e2.MiddleName && 
                e1.LastName == e2.LastName && e1.Login == e2.Login && e1.Password == e2.Password);
        }

        public static bool operator !=(Experimentator e1, Experimentator e2)
        {
            return !(e1.FirstName == e2.FirstName && e1.MiddleName == e2.MiddleName &&
                e1.LastName == e2.LastName && e1.Login == e2.Login && e1.Password == e2.Password);
        }

        public virtual List<Experiment> Experiments { set; get; }
    }
}
