﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ExperimentsDB
{
    public class Experiment
    {
        public Experiment()
        {
            Points = new List<Point>();
            Experimentators = new List<Experimentator>();
        }

        public Experiment(String name, String X, String Y)
        {
            ExperimentName = name;
            LabelX = X;
            LabelY = Y;
            Points = new List<Point>();
            Experimentators = new List<Experimentator>();
        }

        public Experiment(Experiment experiment)
        {
            ExperimentName = experiment.ExperimentName;
            LabelX = experiment.LabelX;
            LabelY = experiment.LabelY;
            Points = experiment.Points;
            Experimentators = experiment.Experimentators;
        }

        public bool Equals(Experiment e)
        {
            return (LabelX == e.LabelX && LabelY == e.LabelY && ExperimentName == e.ExperimentName);
        }

        public static bool operator !=(Experiment e1, Experiment e2)
        {
            return !(e1.LabelX == e2.LabelX && e1.LabelY == e2.LabelY && e1.ExperimentName == e2.ExperimentName);
        }

        public static bool operator ==(Experiment e1, Experiment e2)
        {
            return (e1.LabelX == e2.LabelX && e1.LabelY == e2.LabelY && e1.ExperimentName == e2.ExperimentName);
        }

        [Key]
        public String ExperimentName { set; get; }
        public String LabelX { set; get; }
        public String LabelY { set; get; }

        public virtual List<Point> Points { set; get; }
        public virtual List<Experimentator> Experimentators { set; get; }
    }
}
