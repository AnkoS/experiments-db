﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ExperimentsDB 
{
    public class Context : DbContext
    {
        public Context() : base("ExperimentsDB")
        {

        }

        public DbSet<Point> Points { set; get; }
        public DbSet<Experiment> Experiments { set; get; }
        public DbSet<Experimentator> Experimentators { set; get; }        
    }
}
