﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ExperimentsDB
{
    public class Point
    {
        public Point() { }

        public Point(DateTime dt, double x, double y)
        {
            DateAndTime = dt;
            X = x;
            Y = y;
        }

        public Point(Point point)
        {
            DateAndTime = point.DateAndTime;
            X = point.X;
            Y = point.Y;
        }

        public bool Equals(Point p)
        {
            return (X == p.X && Y == p.Y && DateAndTime == p.DateAndTime);
        }

        public static bool operator !=(Point p1, Point p2)
        {
            return !(p1.X == p2.X && p1.Y == p2.Y && p1.DateAndTime == p2.DateAndTime);
        }

        public static bool operator ==(Point p1, Point p2)
        {
            return (p1.X == p2.X && p1.Y == p2.Y && p1.DateAndTime == p2.DateAndTime);
        }

        [Key]
        public DateTime DateAndTime { set; get; }

        public double X { set; get; }
        public double Y { set; get; }
    }
}
