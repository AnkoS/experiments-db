﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExperimentsDB
{
    public interface IRepository : IDisposable
    {
        void AddPoint(Point point);
        bool AddExperiment(Experiment exp);
        bool AddExperimentator(Experimentator experimentator);

        Point GetPoint(DateTime dt, String name);
        Experiment GetExperiment(String name);
        Experimentator GetExperimentator(String login);

        List<Point> GetPointsOfExperiment(String name);
        List<Experimentator> GetExperimentatorsOfExperiment(String name);
        List<Experiment> GetExperimentsOfExperimentator(String login);

        List<Experiment> GetAllExperiments();

        bool HasTheSameExperimentName(String name);

        void AddPointToExperiment(String name, Point point);
        void AddExperimentToExperimentator(Experimentator experimentator, Experiment experiment);

        void ConnectExperimentAndExperimentator(Experimentator experimentator, Experiment experiment);
        void DisconnectExperimentAndExperimentator(Experimentator experimentator, Experiment experiment);

        bool LoginExists(String login);
        bool PasswordCorrect(String login, String password);

        void DelPointFromExperiment(String name, int n);
        void DelExperiment(String name);
        void DelExperimentator(String login);

        bool IsLeaderOfExperiment(String login, String name);

        void Dispose();

        String GetLabelX(String name);
        String GetLabelY(String name);
    }

    public class Repository : IRepository
    {
        private Context context;

        public Repository()
        {
            context = new Context();
        }

        public void AddPoint(Point point)
        {
            context.Points.Add(point);
            context.SaveChanges();
        }

        public List<Experiment> GetAllExperiments()
        {
            return new List<Experiment>(context.Experiments);
        }

        public bool HasTheSameExperimentName(String name)
        {
            return context.Experiments.Any(experiment => experiment.ExperimentName == name);
        }

        public bool AddExperiment(Experiment exp)
        {
            bool UnicName = !HasTheSameExperimentName(exp.ExperimentName);
            if (UnicName)
            {
                context.Experiments.Add(exp);
                context.SaveChanges();
            }
            return UnicName;
        }

        public void AddExperimentToExperimentator(Experimentator experimentator, Experiment experiment)
        {
            Experimentator e = context.Experimentators.First(exp => exp.Login == experimentator.Login);
            e.Experiments.Add(experiment);
            experiment.Experimentators.Add(e);
            context.SaveChanges();
        }

        public void ConnectExperimentAndExperimentator(Experimentator experimentator, Experiment experiment)
        {
            Experimentator e = context.Experimentators.First(e1 => e1.Login == experimentator.Login);
            Experiment exp = context.Experiments.First(exp1 => exp1.ExperimentName == experiment.ExperimentName);
            e.Experiments.Add(exp);
            exp.Experimentators.Add(e);
            context.SaveChanges();
        }

        public void DisconnectExperimentAndExperimentator(Experimentator experimentator, Experiment experiment)
        {
            Experimentator e = context.Experimentators.First(e1 => e1.Login == experimentator.Login);
            Experiment exp = context.Experiments.First(exp1 => exp1.ExperimentName == experiment.ExperimentName);
            e.Experiments.Remove(exp);
            exp.Experimentators.Remove(e);
            context.SaveChanges();
        }

        public void AddPointToExperiment(String name, Point point)
        {
            Experiment exp = context.Experiments.First(experiment => experiment.ExperimentName == name);
            //context.Points.Attach(point);
            exp.Points.Add(point);
            exp.Points[exp.Points.Count - 1].DateAndTime.AddMilliseconds(exp.Points[exp.Points.Count - 1].DateAndTime.Millisecond - point.DateAndTime.Millisecond);
            context.SaveChanges();
        }

        public Experiment GetExperiment(String name)
        {
            return new Experiment(context.Experiments.First(experiment => experiment.ExperimentName == name));
        }

        public Point GetPoint(DateTime dt, String name)
        {
            Experiment exp = GetExperiment(name);
            return exp.Points.First(point => point.DateAndTime == dt);
        }

        public List<Point> GetPointsOfExperiment(String name)
        {
            return new List<Point>(GetExperiment(name).Points);
        }

        public List<Experimentator> GetExperimentatorsOfExperiment(String name)
        {
            return new List<Experimentator>(GetExperiment(name).Experimentators);
        }

        public Experimentator GetExperimentator(String login)
        {
            return new Experimentator(context.Experimentators.First(experimentator => experimentator.Login == login));
        }

        public List<Experiment> GetExperimentsOfExperimentator(String login)
        {
            return new List<Experiment>(GetExperimentator(login).Experiments);
        }

        public bool LoginExists(String login)
        {
            return context.Experimentators.Any(experimentator => experimentator.Login == login);
        }

        public bool PasswordCorrect(String login, String password)
        {
            return (GetExperimentator(login).Password == password);
        }

        public bool AddExperimentator(Experimentator experimentator)
        {
            bool UnicLogin = !LoginExists(experimentator.Login);
            if (UnicLogin)
            {
                context.Experimentators.Add(experimentator);
                context.SaveChanges();
            }
            return UnicLogin;
        }

        public void DelPointFromExperiment(String name, int n)
        {
            Experiment exp = context.Experiments.First(experiment => experiment.ExperimentName == name);
            Point point = exp.Points[n];

            exp.Points.RemoveAt(n);
            context.Points.Remove(point);
            context.SaveChanges();
        }

        public bool IsLeaderOfExperiment(String login, String name)
        {
            Experiment exp = GetExperiment(name);
            return (exp.Experimentators[0].Login == login);
        }

        public void DelExperimentator(String login)
        {
            if (GetExperimentator(login).Experiments.Count > 0)
            {
                context.Experimentators.Remove(GetExperimentator(login));
                context.SaveChanges();
            }
            //context.Experimentators.Remove(GetExperimentator(login));
        }

        public void DelExperiment(String name)
        {
            Experiment exp = GetExperiment(name);
            if (exp.Experimentators.Count>0)
                exp.Experimentators.Clear();
            /*List<Experimentator> experimentators = GetExperimentatorsOfExperiment(name);
            foreach (Experimentator e in experimentators)
            {
                int n = -1;
                for (int i = 0; i < e.Experiments.Count; i++)
                    if (exp == e.Experiments[i])
                        n = i;
                if (n!=-1)
                    e.Experiments.RemoveAt(n);
            }
            int k = -1;
            for (int i = 0; i < context.Experiments.Count(); i++)
                if (exp == context.Experiments.ToList()[i])
                    k = i;*/
            //context.Experiments.Remove(exp);
            //if (exp.Points.Count > 0)
            //    exp.Points.Remove(exp.Points[0]);
            //context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public String GetLabelX(String name)
        {
            return GetExperiment(name).LabelX;
        }

        public String GetLabelY(String name)
        {
            return GetExperiment(name).LabelY;
        }
    }
}
