﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using ExperimentsGUI;
using ExperimentsDB;
using System.Diagnostics.CodeAnalysis;

namespace IntegrationTests
{
    [TestClass]
    public class ApproximationAlgorythmTests
    {
        [TestMethod]
        [ExcludeFromCodeCoverage]
        public void TestAlgorythmOfApproximationIfResultIsCorrect()
        {
            List<Point> points = new List<Point>();
            points.Add(new Point(DateTime.Now, 0, 0));
            points.Add(new Point(DateTime.Now, 1, 1));
            points.Add(new Point(DateTime.Now, 2, 4));
            int PolDegree = 2;
            Graph graph = new Graph();

            List<double> res = graph.ApplyAlgorythm(points, PolDegree);
            List<double> correctRes = new List<double> { 0, 0, 1 };

            Assert.IsTrue(correctRes.SequenceEqual(res));
        }
    }
}
