﻿namespace ExperimentsGUI
{
    partial class AddExperimentators
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListOfExperimentators = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.DeleteExperimentatorButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.AddExperimentatorButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ListOfExperimentators
            // 
            this.ListOfExperimentators.FormattingEnabled = true;
            this.ListOfExperimentators.Location = new System.Drawing.Point(22, 12);
            this.ListOfExperimentators.Name = "ListOfExperimentators";
            this.ListOfExperimentators.Size = new System.Drawing.Size(159, 121);
            this.ListOfExperimentators.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(195, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "         Логин \r\nэкспериментатора";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(304, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(134, 20);
            this.textBox1.TabIndex = 2;
            // 
            // DeleteExperimentatorButton
            // 
            this.DeleteExperimentatorButton.Location = new System.Drawing.Point(363, 55);
            this.DeleteExperimentatorButton.Name = "DeleteExperimentatorButton";
            this.DeleteExperimentatorButton.Size = new System.Drawing.Size(73, 21);
            this.DeleteExperimentatorButton.TabIndex = 4;
            this.DeleteExperimentatorButton.Text = "Удалить";
            this.DeleteExperimentatorButton.UseVisualStyleBackColor = true;
            this.DeleteExperimentatorButton.Click += new System.EventHandler(this.DeleteExperimentatorButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(198, 112);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(73, 21);
            this.OkButton.TabIndex = 5;
            this.OkButton.Text = "ОК";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(363, 112);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(73, 21);
            this.CancelButton.TabIndex = 6;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // AddExperimentatorButton
            // 
            this.AddExperimentatorButton.Location = new System.Drawing.Point(198, 55);
            this.AddExperimentatorButton.Name = "AddExperimentatorButton";
            this.AddExperimentatorButton.Size = new System.Drawing.Size(75, 23);
            this.AddExperimentatorButton.TabIndex = 7;
            this.AddExperimentatorButton.Text = "Добавить";
            this.AddExperimentatorButton.UseVisualStyleBackColor = true;
            this.AddExperimentatorButton.Click += new System.EventHandler(this.AddExperimentatorButton_Click);
            // 
            // AddExperimentators
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 154);
            this.Controls.Add(this.AddExperimentatorButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.DeleteExperimentatorButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ListOfExperimentators);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddExperimentators";
            this.Text = "Добавление экспериментаторов в эксперимент";
            this.Load += new System.EventHandler(this.AddExperimentators_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ListOfExperimentators;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button DeleteExperimentatorButton;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button AddExperimentatorButton;
    }
}