﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExperimentsDB;

namespace ExperimentsGUI
{
    public partial class Graph : Form
    {
        public Graph()
        {
            InitializeComponent();
        }

        private List<double> GetListX(List<ExperimentsDB.Point> points)
        {
            List<double> x = new List<double>();
            for (int i = 0; i < points.Count; i++)
            {
                x.Add(points[i].X);
            }
            return x;
        }

        private List<double> GetListY(List<ExperimentsDB.Point> points)
        {
            List<double> y = new List<double>();
            for (int i = 0; i < points.Count; i++)
            {
                y.Add(points[i].Y);
            }
            return y;
        }

        public List<double> ApplyAlgorythm(List<ExperimentsDB.Point> points, int PolDegree)
        {
            Approximation Alg = new RegressionApproximation();
            List<double> x = GetListX(points);
            List<double> y = GetListY(points);
            return Alg.Approximate(x, y, PolDegree);
        }

        private void Graph_Load(object sender, EventArgs e)
        {
        }                
    }
}
