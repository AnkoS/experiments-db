﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExperimentsDB;

namespace ExperimentsGUI
{
    public partial class CreateExperiment : Form
    {
        static private String ExperimentName;
        static private String LabelX;
        static private String LabelY;

        public CreateExperiment()
        {
            InitializeComponent();
            ClearEdits();
        }

        private void ClearEdits()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
        }

        public bool IsEmpty()
        {
            return (ExperimentName == "" || LabelX == "" || LabelY == "");
        }

        public String GetName()
        {
            return ExperimentName;
        }

        public String GetLabelX()
        {
            return LabelX;
        }

        public String GetLabelY()
        {
            return LabelY;
        }

        public void ClearExperiment()
        {
            ExperimentName = "";
            LabelX = "";
            LabelY = "";
        }

        private bool HasTheSameName(String name)
        {
            bool b;
            using (Repository rep = new Repository())
                b = rep.HasTheSameExperimentName(name);
            return b;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            ExperimentName = textBox1.Text;
            LabelX = textBox2.Text;
            LabelY = textBox3.Text;
            if (IsEmpty())
            {
                MessageBox.Show("Не все поля введены!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                if (HasTheSameName(GetName()))
                    MessageBox.Show("Данное имя эксперимента уже имеется в базе данных!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    AddExperimentators form = new AddExperimentators();
                    form.ShowDialog();
                    if (form.GetExperimentators().Count != 0)
                        Hide();
                }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            ClearEdits();
            Hide();
        }
    }
}
