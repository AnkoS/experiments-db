﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExperimentsDB;

namespace ExperimentsGUI
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        public bool IsClear()
        {
            return (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "");
        }

        private void ClearEdits()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
        }

        private bool IsExistedLogin()
        {
            bool b;
            using (Repository rep = new Repository())
                b = rep.LoginExists(textBox1.Text);
            return b;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (IsClear())
            {
                MessageBox.Show("Не все поля заполнены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                if (IsExistedLogin())
                {
                    MessageBox.Show("Данный логин уже существует! Введите другой логин!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Experimentator experimentator = new Experimentator
                    (
                        textBox1.Text,
                        textBox2.Text,
                        textBox3.Text,
                        textBox4.Text,
                        textBox5.Text
                    );
                    using (Repository rep = new Repository())
                        rep.AddExperimentator(experimentator);
                    Hide();
                    //MainForm form = new MainForm();
                    //form.Show();
                }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
            //MainForm form = new MainForm();
            //form.Show();
        }
    }
}
