﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExperimentsDB;

namespace ExperimentsGUI
{
    public partial class Choose_CreateExperiment : Form
    {
        static private String ExperientName = "";

        public Choose_CreateExperiment()
        {
            InitializeComponent();
        }

        static public String GetExperimentName()
        {
            return ExperientName;
        }

        private void AddPoint_Load(object sender, EventArgs e)
        {
            IEnumerable<Experiment> experiments = new List<Experiment>();

            using (Repository rep = new Repository())
                experiments = rep.GetExperimentsOfExperimentator(LogIn.GetLogin());
            foreach (Experiment ex in experiments)
            {
                ListOfExperiments.Items.Add(ex.ExperimentName);
            }
        }

        private void CreateExperimentButton_Click(object sender, EventArgs e)
        {
            Hide();
            CreateExperiment form = new CreateExperiment();
            form.ShowDialog();
            Show();
            if (form.IsEmpty())
                return;
            

            Experiment exp = new Experiment {
                ExperimentName = form.GetName(),
                LabelX = form.GetLabelX(),
                LabelY = form.GetLabelY(),
            };
            AddExperimentators form1 = new AddExperimentators();
            List<Experimentator> experimentators = form1.GetExperimentators();
            using (Repository rep = new Repository())
                rep.AddExperimentToExperimentator(experimentators[0], exp);
            for (int i = 1; i < experimentators.Count(); i++)
            {
                using (Repository rep = new Repository())
                    rep.ConnectExperimentAndExperimentator(experimentators[i], exp);
            }
            ListOfExperiments.Items.Add(exp.ExperimentName);
        }

        private void ChooseExperiment_Click(object sender, EventArgs e)
        {
            if (ListOfExperiments.SelectedIndex < 0)
            {
                MessageBox.Show("Выберите эксперимент!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Hide();
            ExperientName = ListOfExperiments.SelectedItem.ToString();
            Graph form = new Graph();
            form.ShowDialog();
            Show();
        }

        private void DelExperimentButton_Click(object sender, EventArgs e)
        {
            if (ListOfExperiments.SelectedIndex < 0)
            {
                MessageBox.Show("Выберите эксперимент!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            String name = ListOfExperiments.SelectedItem.ToString();
            using (Repository rep = new Repository())
            {
                Experiment exp = rep.GetExperiment(name);
                if (exp.Experimentators[0].Login != LogIn.GetLogin())
                {
                    MessageBox.Show("Только создатель эксперимента может удалять эксперимент!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                rep.DelExperiment(name);
            }
            ListOfExperiments.Items.Remove(name);
        }
    }
}
