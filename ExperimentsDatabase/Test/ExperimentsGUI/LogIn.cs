﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExperimentsDB;
using System.Security.Cryptography;

namespace ExperimentsGUI
{
    public partial class LogIn : Form
    {
        static private String Login = "";
        public LogIn()
        {
            InitializeComponent();
        }

        static public String GetLogin()
        {
            return Login;
        }

        public bool IsClearEdits()
        {
            return (textBox1.Text == "" || textBox2.Text == "");
        }

        private void ClearEdits()
        {
            textBox1.Clear();
            textBox2.Clear();
        }

        private bool IsLoginCorret()
        {
            bool b;
            String login = textBox1.Text;
            using (Repository rep = new Repository())
                b = rep.LoginExists(login);
            return b;
        }

        private bool IsPasswordCorrect()
        {
            bool b;
            SHA256CryptoServiceProvider cryptoProvider = new SHA256CryptoServiceProvider();
            var password = Encoding.ASCII.GetString(cryptoProvider.ComputeHash(Encoding.ASCII.GetBytes(textBox2.Text)));
            String login = textBox1.Text;
            using (Repository rep = new Repository())
                b = rep.PasswordCorrect(login, password);
            return b; 
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (IsClearEdits())
            {
                MessageBox.Show("Не все поля введены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!IsLoginCorret() || !IsPasswordCorrect())
            {
                MessageBox.Show("Неверный логин или пароль!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Login = textBox1.Text;
                Hide();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            ClearEdits();
            this.Hide();
            MainForm form = new MainForm();
            form.Show();
        }

        private void LogIn_Load(object sender, EventArgs e)
        {
            ClearEdits();
        }
    }
}
