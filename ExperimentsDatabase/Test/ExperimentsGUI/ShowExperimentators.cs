﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ExperimentsDB;

namespace ExperimentsGUI
{
    public partial class ShowExperimentators : Form
    {
        public ShowExperimentators()
        {
            InitializeComponent();
        }

        private void FillTheTable()
        {
            List<Experimentator> experimentators = new List<Experimentator>();
            String name = Choose_CreateExperiment.GetExperimentName();
            using (Repository rep = new Repository())
            {
                Experiment exp = rep.GetExperiment(name);
                experimentators = exp.Experimentators;
            }
            dataGridView1.RowCount = experimentators.Count;
            for (int i = 0; i < experimentators.Count; i++)
            {
                dataGridView1[0, i].Value = experimentators[i].Login;
                dataGridView1[1, i].Value = experimentators[i].LastName;
                dataGridView1[2, i].Value = experimentators[i].FirstName;
                dataGridView1[3, i].Value = experimentators[i].MiddleName;
            }
            dataGridView1.Rows[0].DefaultCellStyle.BackColor = Color.Blue;
        }

        private void ShowExperimentators_Load(object sender, EventArgs e)
        {
            List<Experimentator> experimentators = new List<Experimentator>();
            String name = Choose_CreateExperiment.GetExperimentName();
            using (Repository rep = new Repository())
            {
                Experiment exp = rep.GetExperiment(name);
                experimentators = exp.Experimentators;
            }
            FillTheTable();
            DelExperimentatorButton.Enabled = (LogIn.GetLogin() == experimentators[0].Login);
            AddExperimentatorButton.Enabled = (LogIn.GetLogin() == experimentators[0].Login);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool IsEmptyEdit()
        {
            return textBox1.Text == "";
        }

        private void AddExperimentatorButton_Click(object sender, EventArgs e)
        {
            if (IsEmptyEdit())
            {
                MessageBox.Show("Введите логин экспериментатора!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            String login = textBox1.Text;

            Experimentator experimentator = new Experimentator();
            using (Repository rep = new Repository())
            {
                if (rep.LoginExists(login))
                {
                    Experiment exp = rep.GetExperiment(Choose_CreateExperiment.GetExperimentName());
                    experimentator = rep.GetExperimentator(login);
                    rep.ConnectExperimentAndExperimentator(experimentator, exp);
                }
                else
                {
                    MessageBox.Show("Экспериментатора с данным логином не существует!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            FillTheTable();
        }

        private void DelExperimentatorButton_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите строку с экспериментатором!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewSelectedRowCollection rows = dataGridView1.SelectedRows;
                String name = Choose_CreateExperiment.GetExperimentName();
                Experiment experiment;

                if (rows[0] == dataGridView1.Rows[0])
                {
                    MessageBox.Show("Нельзя удалить организатора эксперимента!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                using (Repository rep = new Repository())
                {
                    experiment = rep.GetExperiment(name);
                }
                foreach (DataGridViewRow row in rows)
                {
                    Experimentator experimentator;
                    String login = row.Cells[0].Value.ToString();
                    using (Repository rep = new Repository())
                    {
                        experimentator = rep.GetExperimentator(login);
                        rep.DisconnectExperimentAndExperimentator(experimentator, experiment);
                    }
                }
                FillTheTable();
            }
        }
    }
}
