﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentsGUI
{
    public abstract class Approximation
    {
        public abstract List<double> Approximate(List<double> X, List<double> Y, int PolDegree);
    }

    public class RegressionApproximation : Approximation
    {
        private double Sum(List<double> X, int n1, int n2)
        {
            double Res = 0;
            for (int i = 0; i < n1; i++)
            {
                Res += Math.Pow(X[i], n2);
            }
            return Res;
        }

        private double Sum1(List<double> X, List<double> Y, int n1, int n2)
        {
            double Res = 0;
            for (int i = 0; i < n1; i++)
            {
                Res += Y[i] * Math.Pow(X[i], n2);
            }
            return Res;
        }

        private void Gauss(List<double> A, double[,] K, int n)
        {
            for (int i = 0; i <= n; i++)
            {
                for (int j = 0; j <= n - i; j++)
                    for (int l = n + 1; l >= i; l--)
                        K[j, l] /= K[j, i];
                for (int j = 0; j < n - i; j++)
                    for (int l = 0; l <= n + 1; l++)
                        K[j, l] -= K[n - i, l];
            }

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= i; j++)
                    K[i, n + 1] -= K[i - j, n + 1] * K[i, n - i + j];
                for (int l = n - i + 1; l <= n; l++)
                    K[i, l] = 0;
            }

            for (int i = 0; i <= n; i++)
                A.Add(K[n - i, n + 1]);
        }

        public override List<double> Approximate(List<double> X, List<double> Y, int PolDegree)
        {
            if ((X == null) || (Y == null))
                throw new ArgumentNullException("Ошибка! Список является ссылкой на null!");
            if ((X.Count == 0) || (Y.Count == 0))
                throw new ArgumentException("Ошибка! Список координат пустой!");
            if (PolDegree < 1)
                throw new ArgumentException("Ошибка! Степень полинома должна быть положительной!");
            if (X.Count != Y.Count)
                throw new ArgumentException("Ошибка! Количество координат X и Y должно совпадать!");
            if (PolDegree >= X.Count)
                throw new ArgumentException("Ошибка! Степень полинома должна быть меньше количества точек!");

            int N = X.Count;
            List<double> A = new List<double>();
            double[,] K = new double[PolDegree+1, PolDegree+2];
            for (int i = 0; i <= PolDegree; i++)
            {
                for (int j = 0; j <= PolDegree; j++)
                    K[i, j] = Sum(X, N, i + j);
                K[i, PolDegree + 1] = Sum1(X, Y, N, i);
            }
            Gauss(A, K, PolDegree);
            return A;
        }
    }
}
