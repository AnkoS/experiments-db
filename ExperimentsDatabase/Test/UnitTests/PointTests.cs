﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExperimentsDB;

namespace UnitTests
{
    [TestClass]
    public class PointTests
    {
        [TestMethod]
        public void TestPointIfContstructorWorksCorrect()
        {
            DateTime dt = DateTime.Now;
            double x = 0;
            double y = 0;
            Point p = new Point(dt, x, y);

            Assert.IsTrue((p.DateAndTime == dt) && (p.X == x) && (p.Y == y));
        }

        [TestMethod]
        public void TestPointIfEqual()
        {
            DateTime dt = DateTime.Now;
            Point p1 = new Point(dt, 0, 0);
            Point p2 = new Point(dt, 0, 0);

            Assert.IsTrue(p1 == p2);
        }

        [TestMethod]
        public void TestPointIfNotEqual2()
        {
            DateTime dt = DateTime.Now;
            Point p1 = new Point(dt, 0, 0);
            Point p2 = new Point(dt, 1, 0);

            Assert.IsTrue(p1 != p2);
        }

        [TestMethod]
        public void TestPointIfEqual2()
        {
            DateTime dt = DateTime.Now;
            Point p1 = new Point(dt, 0, 0);
            Point p2 = new Point(dt, 0, 0);

            Assert.IsTrue(p1.Equals(p2));
        }
    }
}
