﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExperimentsDB;

namespace ExperimentsGUI
{
    public partial class AddPoint : Form
    {
        static private double X;
        static private double Y;
        static private bool clear = true;

        public AddPoint()
        {
            InitializeComponent();
        }

        public double GetX()
        {
            return X;
        }

        public double GetY()
        {
            return Y;
        }

        private bool IsEmpty()
        {
            return (textBox1.Text == "" || textBox2.Text == "");
        }

        public bool IsClear()
        {
            return clear;
        }

        private void ClearEdits()
        {
            textBox1.Clear();
            textBox2.Clear();
        }

        private bool IsCorrect()
        {
            double tmp;
            return (double.TryParse(textBox1.Text, out tmp) && double.TryParse(textBox2.Text, out tmp));
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (IsEmpty())
            {
                MessageBox.Show("Не все поля заполнены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                if (!IsCorrect())
                {
                    MessageBox.Show("Некорректные значения координаты точки!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (textBox1.Text.Contains("."))
                        X = double.Parse(textBox1.Text.Replace(".", ","));
                    else
                        X = double.Parse(textBox1.Text);
                    if (textBox2.Text.Contains("."))
                        Y = double.Parse(textBox2.Text.Replace(".", ","));
                    else
                        Y = double.Parse(textBox2.Text);
                    clear = false;
                    this.Hide();
                }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            ClearEdits();
            clear = true;
            this.Hide();
        }

        private void AddPoint_Load(object sender, EventArgs e)
        {
            using (Repository rep = new Repository())
            {
                String name = Choose_CreateExperiment.GetExperimentName();
                groupBox1.Text = rep.GetLabelX(name);
                groupBox2.Text = rep.GetLabelY(name);
            }
        }
    }
}
