﻿namespace ExperimentsGUI
{
    partial class Choose_CreateExperiment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListOfExperiments = new System.Windows.Forms.ListBox();
            this.ChooseExperiment = new System.Windows.Forms.Button();
            this.CreateExperimentButton = new System.Windows.Forms.Button();
            this.DelExperimentButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ListOfExperiments
            // 
            this.ListOfExperiments.FormattingEnabled = true;
            this.ListOfExperiments.Location = new System.Drawing.Point(12, 12);
            this.ListOfExperiments.Name = "ListOfExperiments";
            this.ListOfExperiments.Size = new System.Drawing.Size(353, 186);
            this.ListOfExperiments.TabIndex = 0;
            // 
            // ChooseExperiment
            // 
            this.ChooseExperiment.Location = new System.Drawing.Point(12, 220);
            this.ChooseExperiment.Name = "ChooseExperiment";
            this.ChooseExperiment.Size = new System.Drawing.Size(94, 40);
            this.ChooseExperiment.TabIndex = 1;
            this.ChooseExperiment.Text = "Выбрать\r\nэксперимент";
            this.ChooseExperiment.UseVisualStyleBackColor = true;
            this.ChooseExperiment.Click += new System.EventHandler(this.ChooseExperiment_Click);
            // 
            // CreateExperimentButton
            // 
            this.CreateExperimentButton.Location = new System.Drawing.Point(271, 220);
            this.CreateExperimentButton.Name = "CreateExperimentButton";
            this.CreateExperimentButton.Size = new System.Drawing.Size(94, 40);
            this.CreateExperimentButton.TabIndex = 2;
            this.CreateExperimentButton.Text = "Создать эксперимент";
            this.CreateExperimentButton.UseVisualStyleBackColor = true;
            this.CreateExperimentButton.Click += new System.EventHandler(this.CreateExperimentButton_Click);
            // 
            // DelExperimentButton
            // 
            this.DelExperimentButton.Location = new System.Drawing.Point(139, 220);
            this.DelExperimentButton.Name = "DelExperimentButton";
            this.DelExperimentButton.Size = new System.Drawing.Size(94, 40);
            this.DelExperimentButton.TabIndex = 3;
            this.DelExperimentButton.Text = "Удалить эксперимент";
            this.DelExperimentButton.UseVisualStyleBackColor = true;
            this.DelExperimentButton.Click += new System.EventHandler(this.DelExperimentButton_Click);
            // 
            // Choose_CreateExperiment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 272);
            this.Controls.Add(this.DelExperimentButton);
            this.Controls.Add(this.CreateExperimentButton);
            this.Controls.Add(this.ChooseExperiment);
            this.Controls.Add(this.ListOfExperiments);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Choose_CreateExperiment";
            this.Text = "Выбор эксперимента";
            this.Load += new System.EventHandler(this.AddPoint_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox ListOfExperiments;
        private System.Windows.Forms.Button ChooseExperiment;
        private System.Windows.Forms.Button CreateExperimentButton;
        private System.Windows.Forms.Button DelExperimentButton;
    }
}