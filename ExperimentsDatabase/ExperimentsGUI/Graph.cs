﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExperimentsDB;

namespace ExperimentsGUI
{
    public partial class Graph : Form
    {
        List<double> A = new List<double>();
        int PolDegree;
        List<ExperimentsDB.Point> points = new List<ExperimentsDB.Point>();

        private double MinStep()
        {
            List<double> x = new List<double>();
            for (int i = 0; i < points.Count; i++)
                x.Add(points[i].X);
            x.Sort();
            double tmp = x[1]-x[0];
            for (int i = 2; i < x.Count; i++)
            {
                double tmp1 = x[i] - x[i-1];
                if (tmp1 < tmp && tmp1!=0 || tmp==0)
                    tmp = tmp1;
            }
            return tmp;
        }

        private double MinX()
        {
            double min = points[0].X;
            for (int i = 0; i < points.Count; i++)
                if (points[i].X < min)
                    min = points[i].X;
            return min;
        }

        private double MaxX()
        {
            double max = points[0].X;
            for (int i = 0; i < points.Count; i++)
                if (points[i].X > max)
                    max = points[i].X;
            return max;
        }

        public Graph()
        {
            InitializeComponent();
        }

        private void FillTheTable()
        {
            if (points.Count > 0)
                dataGridView1.RowCount = points.Count;
            else
            {
                dataGridView1.RowCount = 1;
                dataGridView1[0, 0].Value = "";
                dataGridView1[1, 0].Value = "";
                dataGridView1[2, 0].Value = "";
                return;
            }
            String Day, Month, Year, Hours, Minutes, Seconds;
            for(int i = 0; i < points.Count; i++)
            {
                Day = points[i].DateAndTime.Date.Day.ToString();
                Month = points[i].DateAndTime.Date.Month.ToString();
                Year = points[i].DateAndTime.Date.Year.ToString();
                Hours = points[i].DateAndTime.TimeOfDay.Hours.ToString();
                Minutes = points[i].DateAndTime.TimeOfDay.Minutes.ToString();
                Seconds = points[i].DateAndTime.TimeOfDay.Seconds.ToString();
                String tmp = points[i].DateAndTime.ToString("dd.MM.yyyy hh:mm:ss.fff tt");
                if (Day.Length < 2)
                    Day = "0"+Day;
                if (Month.Length < 2)
                    Month = "0" + Month;
                dataGridView1[0, i].Value = points[i].X;
                dataGridView1[1, i].Value = points[i].Y;
                dataGridView1[2, i].Value = Day+"."+Month+"."+Year + " " + Hours+":"+Minutes+":"+Seconds;
                dataGridView1[0, i].Tag = tmp;
                dataGridView1[1, i].Tag = points[i].DateAndTime.Hour;
                dataGridView1[2, i].Tag = points[i].DateAndTime.Millisecond;
            }
            //DrawPoints(Color.Red);
        }

        private void Input(List<double> X, List<double> Y)
        {
            PolDegree = int.Parse(numericUpDown1.Value.ToString());
        }

        private double Func(double X, List<double> A, int n)
        {
            double res = 0, tmp;
            for (int i = 0; i <= n; i++)
            {
                tmp = A[i];
                for (int j = 0; j < i; j++)
                    tmp *= X;
                res += tmp;
            }
            return res;
        }

        private void DrawPoints(Color PointsColor)
        {
            chart1.Series["ExperimentPoints"].Color = PointsColor;
            chart1.Series["ExperimentPoints"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;

            for (int i = 0; i < points.Count; i++)
                chart1.Series["ExperimentPoints"].Points.AddXY(points[i].X, points[i].Y);
        }

        private void DrawGraph(List<double> A, int PolDegree, double X0, double Xn, Color LineColor)
        {
            int n = 200;
            double h = (Xn - X0) / n;
            double tmp = MinStep();

            chart1.Series["ExperimentGraph"].Color = LineColor;
            chart1.Series["ExperimentGraph"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

            for (double x = X0-tmp; x <= Xn+tmp; x += h)
                chart1.Series["ExperimentGraph"].Points.AddXY(x, Func(x, A, PolDegree));
        }

        private void Draw(List<double> A, int PolDegree, double X0, double Xn, Color LineColor, Color PointsColor)
        {
            if (A.Count>0)
                DrawGraph(A, PolDegree, MinX(), MaxX(), LineColor);
            DrawPoints(PointsColor);
        }

        private void ClearGraph(List<double> A, int PolDegree)
        {
            Draw(A, PolDegree, MinX(), MaxX(), Color.White, Color.White);
            chart1.Series["ExperimentPoints"].Points.Clear();
            chart1.Series["ExperimentGraph"].Points.Clear();
        }

        private bool EHighPolDegree()
        {
            return (points.Count <= int.Parse(numericUpDown1.Value.ToString()));
        }

        private bool ENotEnoughPoints()
        {
            return (points.Count < 2);
        }

        private int GetPolDegree()
        {
            return int.Parse(numericUpDown1.Value.ToString());
        }

        private List<double> GetListX(List<ExperimentsDB.Point> points)
        {
            List<double> x = new List<double>();
            for (int i = 0; i < points.Count; i++)
            {
                x.Add(points[i].X);
            }
            return x;
        }

        private List<double> GetListY(List<ExperimentsDB.Point> points)
        {
            List<double> y = new List<double>();
            for (int i = 0; i < points.Count; i++)
            {
                y.Add(points[i].Y);
            }
            return y;
        }

        private void DrawGrapsButton_Click(object sender, EventArgs e)
        {
            if (EHighPolDegree())
            {
                MessageBox.Show("Степень полинома должна быть меньше количества точек в эксперименте!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (ENotEnoughPoints())
            {
                MessageBox.Show("Для построения графика должно быть не менее 2 точек!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ClearGraph(A, PolDegree);
            PolDegree = GetPolDegree();

            A = ApplyAlgorythm(points, PolDegree);
            Draw(A, PolDegree, MinX(), MaxX(), Color.Blue, Color.Red);
        }

        public List<double> ApplyAlgorythm(List<ExperimentsDB.Point> points, int PolDegree)
        {
            Approximation Alg = new RegressionApproximation();
            List<double> x = GetListX(points);
            List<double> y = GetListY(points);
            return Alg.Approximate(x, y, PolDegree);
        }

        private void Graph_Load(object sender, EventArgs e)
        {
            chart1.Series[0].LegendText = "График функции";
            chart1.Series[1].LegendText = "Точки из эксперимента";
            using (Repository rep = new Repository())
            {
                String name = Choose_CreateExperiment.GetExperimentName();
                points = rep.GetPointsOfExperiment(name);
                label2.Text = rep.GetLabelX(name);
                label3.ForeColor = Color.White;
                label3.Text = "";
                dataGridView1.Columns[0].HeaderText = rep.GetLabelX(name);
                dataGridView1.Columns[1].HeaderText = rep.GetLabelY(name);
            }
            FillTheTable();

            if (ENotEnoughPoints())
            {
                DrawPoints(Color.Red);
                return;
            }

            //ClearGraph(A, PolDegree);
            PolDegree = GetPolDegree();

            Approximation Alg = new RegressionApproximation();
            List<double> x = GetListX(points);
            List<double> y = GetListY(points);
            A = Alg.Approximate(x, y, PolDegree);
            Draw(A, PolDegree, MinX(), MaxX(), Color.Blue, Color.Red);
        }

        private void AddPointButton_Click(object sender, EventArgs e)
        {
            AddPoint form = new AddPoint();
            form.ShowDialog();
            if (form.IsClear())
                return;
            ExperimentsDB.Point point = new ExperimentsDB.Point(DateTime.Now, form.GetX(), form.GetY());
            String name = Choose_CreateExperiment.GetExperimentName();
            using (Repository rep = new Repository())
            {
                rep.AddPointToExperiment(name, point);
            }
            points.Add(point);
            FillTheTable();

            PolDegree = GetPolDegree();

            if (points.Count() != 0)
            {
                ClearGraph(A, PolDegree);
            }

            if (EHighPolDegree() || ENotEnoughPoints())
            {
                DrawPoints(Color.Red);
                return;
            }

            A = ApplyAlgorythm(points, PolDegree);
            Draw(A, PolDegree, MinX(), MaxX(), Color.Blue, Color.Red);
        }

        private void DeletePointButton_Click(object sender, EventArgs e)
        {
            int n = dataGridView1.CurrentRow.Index;
            if (n < 0)
            {
                MessageBox.Show("Выберите точку для удаления!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ClearGraph(A, PolDegree);
            
            DateTime dt = DateTime.Parse(dataGridView1[0, n].Tag.ToString());
            double tmp = double.Parse(dataGridView1[1, n].Tag.ToString());
            //dt.AddMilliseconds(tmp);
            dt = dt.AddHours(tmp - dt.Hour);
            String name = Choose_CreateExperiment.GetExperimentName();
            using (Repository rep = new Repository())
            {
                ExperimentsDB.Point p = rep.GetPoint(dt, name);
                points.RemoveAt(n);
                rep.DelPointFromExperiment(name, n);
                points = rep.GetPointsOfExperiment(name);
            }
            FillTheTable();

            PolDegree = GetPolDegree();

            if (EHighPolDegree() || points.Count == 0)
            {
                // Сделать что-нибудь со степенью полинома
                return;
            }

            if (ENotEnoughPoints())
            {
                DrawPoints(Color.Red);
                return;
            }            

            A = ApplyAlgorythm(points, PolDegree);
            Draw(A, PolDegree, MinX(), MaxX(), Color.Blue, Color.Red);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int OldDegree = PolDegree;
            PolDegree = GetPolDegree();

            if (ENotEnoughPoints())
            {
                DrawPoints(Color.Red);
                //MessageBox.Show("Для построения графика должно быть не менее 2 точек!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (EHighPolDegree())
            {
                MessageBox.Show("Степень полинома должна быть меньше количества точек в эксперименте!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                PolDegree = OldDegree;
                numericUpDown1.Value = OldDegree;
                return;
            }

            if (A.Count>0)
                ClearGraph(A, OldDegree);

            A = ApplyAlgorythm(points, PolDegree);
            Draw(A, PolDegree, MinX(), MaxX(), Color.Blue, Color.Red);
        }

        private void ShowExperimentatorsButton_Click(object sender, EventArgs e)
        {
            ShowExperimentators form = new ShowExperimentators();
            form.ShowDialog();
        }

        private void label3_Paint(object sender, PaintEventArgs e)
        {
            String Name;
            using (Repository rep = new Repository())
            {
                Name = rep.GetLabelY(Choose_CreateExperiment.GetExperimentName());

            }
            var g = e.Graphics;
            g.DrawString(Name, new Font("Tahoma", 8), Brushes.Black, 0, 0,
            new StringFormat(StringFormatFlags.DirectionVertical));
            //label3.BackColor = Color.Gray;
        }
    }
}
