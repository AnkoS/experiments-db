﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ExperimentsDB;

namespace ExperimentsGUI
{
    public partial class AddExperimentators : Form
    {
        static private List<Experimentator> experimentators = new List<Experimentator>();

        public AddExperimentators()
        {
            InitializeComponent();
        }

        private void ClearList()
        {
            ListOfExperimentators.Items.Clear();
        }

        private bool IsEmptyLogin()
        {
            return (textBox1.Text == "");
        }

        private String GetLogin()
        {
            return textBox1.Text;
        }

        private bool LoginInList(String login)
        {
            for (int i = 0; i < ListOfExperimentators.Items.Count; i++)
            {
                if (login == ListOfExperimentators.Items[i].ToString())
                    return true;
            }
            return false;
        }

        private String GetLogin(int index)
        {
            return ListOfExperimentators.Items[index].ToString();
        }

        private void ClearLogin()
        {
            textBox1.Clear();
        }

        public List<Experimentator> GetExperimentators()
        {
            return experimentators;
        }

        private bool IsLoginExists(String login)
        {
            bool b;
            using (Repository rep = new Repository())
                b = rep.LoginExists(login);
            return b;
        }

        private void AddExperimentatorButton_Click(object sender, EventArgs e)
        {
            if (IsEmptyLogin())
            {
                MessageBox.Show("Введите логин экспериментатора!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                if (!IsLoginExists(GetLogin()))
                {
                    MessageBox.Show("Экспериментаторов с данным логином не существует!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    if (LoginInList(textBox1.Text))
                    {
                        MessageBox.Show("Экспериментатор с данным логином уже добавлен в список!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        ListOfExperimentators.Items.Add(GetLogin());
                        using (Repository rep = new Repository())
                        {
                            experimentators.Add(rep.GetExperimentator(GetLogin()));
                        }
                        ClearLogin();
                    }
        }

        private void DeleteExperimentatorButton_Click(object sender, EventArgs e)
        {
            if (ListOfExperimentators.SelectedIndex > 0)
                ListOfExperimentators.Items.RemoveAt(ListOfExperimentators.SelectedIndex);
            else
            if (ListOfExperimentators.SelectedIndex == 0)
            {
                MessageBox.Show("Нельзя удалить создателя эксперимента!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Выберите экспериментатора для удаления!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            Hide();
            //Choose_CreateExperiment form = new Choose_CreateExperiment();
            //form.ShowDialog();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            experimentators.Clear();
            ClearLogin();
            ClearList();
            Hide();
            //Choose_CreateExperiment form = new Choose_CreateExperiment();
            //form.ShowDialog();
        }

        private void AddExperimentators_Load(object sender, EventArgs e)
        {
            experimentators.Clear();
            ListOfExperimentators.Items.Add(LogIn.GetLogin());
            using (Repository rep = new Repository())
                experimentators.Add(rep.GetExperimentator(LogIn.GetLogin()));
        }
    }
}
