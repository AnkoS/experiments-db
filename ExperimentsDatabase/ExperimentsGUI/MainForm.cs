﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExperimentsDB;

namespace ExperimentsGUI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            Hide();
            LogIn form1 = new LogIn();
            form1.ShowDialog();
            if (form1.IsClearEdits())
                return;
            Choose_CreateExperiment form = new Choose_CreateExperiment();
            form.ShowDialog();
            Show();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            Hide();
            Register form = new Register();
            form.ShowDialog();
            //CloseAll();
            Show();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}
