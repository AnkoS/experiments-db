﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExperimentsDB;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;

namespace IntegrationTests
{
    [TestClass]
    public class RepositoryTests
    {
        [TestMethod]
        public void TestAddExperimentInDB()
        {
            IRepository repository = new Repository();
            string name = "Test experiment";
            Experiment exp = new Experiment(name, "X", "Y");

            repository.AddExperiment(exp);
            Experiment resExp = repository.GetExperiment(name);

            Assert.IsTrue(exp == resExp);

            repository.DelExperiment(name);
        }

        [TestMethod]
        public void TestAddExperimentatorInDB()
        {
            IRepository repository = new Repository();
            Experimentator experimentator = new Experimentator("a", "a", "a", "a", "abc");

            repository.AddExperimentator(experimentator);
            Experimentator resExperimentator = repository.GetExperimentator("a");

            Assert.IsTrue(experimentator == resExperimentator);

            repository.DelExperimentator("a");
        }


        [TestMethod]
        public void TestAddPointToExperimentInDB()
        {
            IRepository repository = new Repository();
            DateTime dt = DateTime.Now;
            Point point = new Point(dt, 0, 0);
            string name = "Test experiment";
            Experiment exp = new Experiment(name, "X", "Y");

            repository.AddExperiment(exp);
            repository.AddPointToExperiment(name, point);
            Point resPoint = repository.GetPoint(dt, name);

            Assert.IsTrue(point == resPoint);
        }

        [TestMethod]
        public void TestIsConsistUnicExperimentName()
        {
            IRepository repository = new Repository();
            string name = "Test experiment";
            Experiment exp = new Experiment(name, "X", "Y");

            repository.AddExperiment(exp);

            Assert.IsTrue(repository.HasTheSameExperimentName(name));

            repository.DelExperiment(name);
        }

        [TestMethod]
        public void TestIsConsistUnicExperimentatorLogin()
        {
            IRepository repository = new Repository();
            string login = "a";
            Experimentator experimentator = new Experimentator(login, "a", "a", "a", "abc");

            repository.AddExperimentator(experimentator);

            Assert.IsTrue(repository.LoginExists("a"));

            repository.DelExperimentator("a");
        }

        [TestMethod]
        public void TestIsPasswordCorrect()
        {
            IRepository repository = new Repository();
            String login = "a";
            String password = "abc";
            Experimentator experimentator = new Experimentator(login, "a", "a", "a", password);

            SHA256CryptoServiceProvider cryptoProvider = new SHA256CryptoServiceProvider();
            var p = Encoding.ASCII.GetString(cryptoProvider.ComputeHash(Encoding.ASCII.GetBytes(password)));

            repository.AddExperimentator(experimentator);

            Assert.IsTrue(repository.PasswordCorrect(login, p));

            repository.DelExperimentator(login);
        }

        [TestMethod]
        public void TestIsLabelXCorrect()
        {
            IRepository repository = new Repository();
            string name = "Test experiment";
            Experiment exp = new Experiment(name, "X", "Y");

            repository.AddExperiment(exp);
            string X = repository.GetLabelX(name);

            Assert.IsTrue(X == "X");

            repository.DelExperiment(name);
        }

        [TestMethod]
        public void TestIsLabelYCorrect()
        {
            IRepository repository = new Repository();
            string name = "Test experiment";
            Experiment exp = new Experiment(name, "X", "Y");

            repository.AddExperiment(exp);
            string Y = repository.GetLabelY(name);

            Assert.IsTrue(Y == "Y");

            repository.DelExperiment(name);
        }

        [TestMethod]
        public void TestIsExperimentConsistCorrectPoints()
        {

            IRepository repository = new Repository();
            string name = "Experimentt2";
            DateTime dt1 = DateTime.Now;
            DateTime dt2 = DateTime.Now;
            dt2 = dt2.AddHours(2);
            DateTime dt3 = DateTime.Now;
            dt3 = dt3.AddHours(4);
            Experiment exp = new Experiment(name, "X", "Y");
            Point p1 = new Point(dt1, 0, 0);
            Point p2 = new Point(dt2, 1, 1);
            Point p3 = new Point(dt3, 2, 2);

            repository.AddExperiment(exp);
            repository.AddPointToExperiment(name, p1);
            repository.AddPointToExperiment(name, p2);
            repository.AddPointToExperiment(name, p3);

            List<Point> points = repository.GetPointsOfExperiment(name);

            Assert.IsTrue(points.Count == 3 && points[0] == p1 && points[1] == p2 && points[2] == p3);
        }
    }


}
